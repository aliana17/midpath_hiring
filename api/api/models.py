from sqlalchemy import text
from flask import request

from api.db import db

class Inventory(db.Model):
    __tablename__ = "Inventory"
    #Fields
    id = db.Column(db.Integer, primary_key=True)
    item_name = db.Column(db.String)
    quantity = db.Column(db.Integer)#

#db.create_all()
