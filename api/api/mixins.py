klassMap = {}
known_attrs = {}


def register(klass):
    """
    Register classes as endpoints for views.
    """
    global klassMap  # pylint: disable=global-statement
    klass.verify()
    if klass.slug in klassMap:
        raise ValueError()
    klassMap[klass.slug] = klass
    return klass


def check_mixin(mixin):
    """
    Checks for mixins at declaration time.
    """
    global known_attrs  # pylint: disable=global-statement
    for attr in dir(mixin):
        if (
            not attr.startswith("__")
            and not attr.endswith("__")
            and not attr == "verify"
            and not callable(getattr(mixin, attr))
        ):
            if attr in known_attrs:
                raise ValueError(f"{attr} already present in {known_attrs[attr]}")
            known_attrs[attr] = mixin
    return mixin


@check_mixin
class BaseMixin:
    @classmethod
    def verify(cls):
        assert hasattr(cls, "slug")
